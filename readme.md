# SupeRSS Application ##



## General information ##

- Url: [http://superss.fruitygames.fr](http://superss.fruitygames.fr)
- Time spent: <24 hours
- Technologies used:
-- Node.js / Express / MongoDB (Mongoose) / Feedparser
-- React.js / Flux / Sass
-- Nginx (my host server)
-- Git / Bitbucket
-- Gimp (for this wonderful logo)

## Setup ##

* Go to the root of the project
* run `npm install`
* run `gulp setup` (assuming you have gulp installed globally)
* Make sure `MongoDB` is installed (latest version is fine) and is running.
* Run `node app` and go to [http://localhost:3108](http://localhost:3108)


## Known bugs / limitations ## 

### Unread article issue ###
When clicking on an article, then going to the "Unread" section, the article is still here (even if it should be read, as it was clicked). Refresh the page make it work.

### Starred articles ###
Starred filter is not working (It's the same as "All"). Feature is not missing because of complexity, but because of time. To do it, we need to create a button, that will publish an event to the ArticleStore, update the localstorage, publish the store state through another event, etc...

### No user management system ###
Once more, because of time. If you want to have a look at an in-house user management system that I made (with levels, xp and mana ;). And soon RPG-like status, achievements and daily quest!) please go to [http://tuktuk.gg](http://tuktuk.gg).

### Cannot add dynamically new feeders ###
Should not be hard as well: adding a new text input, on Submit call some route (let's say `/feeder/add`) that will call the addFeeder method of the server's FeedersController (like it's done for the initialization).

### Client cannot sort/filter by name/feed/update ###
To be honnest, I did not know where to put those buttons...

### Cannot filter by feeder ###
We could add this feature. Click on a feeder will toggle it, making the news visible/hidden. Should be like a filter, but on a 2nd dimension.

### Accessibility was done... Blindly. ###
I did not run NVDA to check that the components were fully accessible. I tried to use as much semantic as possible (`<time>`, proper heading structure, lists structures, `section`, `article`,...), putting alt to the only image that I have,...