/*
  Copyright (C) 2015 Jérémy GABRIELE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
  CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

var Article     = require('../models/Article');

var FeedHelper = require('../helpers/FeedFetchHelper'); 


/*
 * Save the list of Article Mongoose objects in DB... Recursively.
 * @param articleInstances: array of Article Mongoose objects
 * @param callback : function(success) to call at the end of the processing
 */
function _saveArticlesRecursively(articleInstances, callback) {
  if (!articleInstances.length) {
    // No more to process. We can call the callback;
    if (callback) callback(true);
    return;
  }

  var firstElement = articleInstances.shift(); // Take the first element of the array, and pop() it.
  firstElement.save(function(err, article) {
    if (err) {
      if (err.code !== 11000) { // 11000 => "Duplicate entry"
        console.error(err);
      }
    }

    _saveArticlesRecursively(articleInstances, callback);
  });

}

/*
 * Fetch data from feed urls and create Article documents
 * @param feedUrls: array of feeds {name:string, url:string}
 * @param callback: function(success) to call at the end of the processing
 */
exports.fetchDataAndCreateArticles = fetchDataAndCreateArticles = function(feeders, callback) {
  if (!feeders.length) {
    if (callback) callback(true);
    return;
  }

  feeders.forEach(function(feeder) {
    FeedHelper.fetchFeed(feeder.url, function(allFeeds) {

      if (!allFeeds && callback) callback(false); // Not success
      
      var articleInstance = null;
      var articleInstances = allFeeds.map(function(feedData, index) {
        return new Article({
          guid        : feedData.guid,
          feeder      : feeder.name,
          title       : feedData.title,
          author      : feedData.author,
          description : feedData.description,
          summary     : feedData.summary,
          date        : feedData.date,
          link        : feedData.link,
          image       : feedData.image.url
        });
      });

      _saveArticlesRecursively(articleInstances, callback);
    });
  })
}

/*
 * get every article in the DB
 * @param callback: function(err, allArticles) to call at the end of the processing
 */
exports.getAllArticles = function(callback) {
  Article.find({}).sort('-date').exec(callback);
}

/*
 * get every article in the DB
 * @param callback: function(err, allArticles) to call at the end of the processing
 */
exports.getArticlesFromFeeder = function(feeder, callback) {
  Article.find({feeder: feeder})
}


/*
 * remove every article in the DB
 * @param callback: function(err, allArticles) to call at the end of the processing
 */
exports.removeAllArticles = function(callback) {
  Article.remove({}, callback);
}

setTimeout(function() {
  FeedersController.getAllFeeders(function(err, allFeeders) {
    var allFeedersObjects = allFeeders.map(function(feeder) { return {name: feeder.name, url: feeder.url} });
    ArticlesController.fetchDataAndCreateArticles(allFeedersObjects, function(success) {
      res.render('index');
    });
  });
}, 60 * 60 * 1000)