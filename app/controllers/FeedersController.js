/*
  Copyright (C) 2015 Jérémy GABRIELE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
  CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

var Feeder = require('../models/Feeder');


/*
 * Add a feeder in DB
 * @param name, url: name and url of the feed. Both are unique.
 * @param callback: function(success) to call at the end of the processing
 */
exports.addFeeder = function(name, url, callback) {
  Feeder.count({}, function(count) {
    if (count <= 6) {
      var f = new Feeder({name: name, url: url});
      f.save(callback);
    } else {
      console.error('No more than 6 feeds');
    }
  })
}


/*
 * Fetch feeders data from DB
 * @param callback: function(err, arrayOfFeeders) to call at the end of the processing
 */
exports.getAllFeeders = function(callback) {
  Feeder.find({}, callback);
}