/*
  Copyright (C) 2015 Jérémy GABRIELE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
  CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

var React = require('react');

var Feed        = require('components/FeedersList/Feed/Feed.jsx');
var FiltersList = require('components/FiltersList/FiltersList.jsx');

var FeederStore   = require('stores/FeederStore');

var mixins = require('scripts/mixins.js');

var FeedersList = React.createClass({
  mixins: [mixins.FilterManagementMixin],
  propTypes: {
    feeders: React.PropTypes.array.isRequired
  },
  getInitialState: function() {
    return {
      feeders: this.props.feeders
    }
  },
  handleFeederUpdate: function(StoreState) {
    this.setState({
      feeders: StoreState.feedersFullList
    });
  },
  componentDidMount: function() {
    // When a feeder has been added (or when the store inits), we want to (re)render the component
    FeederStore.addFeedersUpdatedListener(this.handleFeederUpdate);
  },
  componentWillUnmount: function() {
    FeederStore.removeFeedersUpdatedListener(this.handleFeederUpdate);
  },
  render: function() {
    var allFeeds = this.state.feeders.map(function(feeder) {
      return (<Feed key = { feeder.name }
                   name = { feeder.name } />);
    });
    return (
      <div className="feeders-list">
        <h2 className="hidden">Feeders list</h2>
        <header>
          <img src="/images/superss_logo.png" alt="SupeRSS logo" />
        </header>
        <div>
          <section className="all-feeds">
            { allFeeds }
          </section>
          <FiltersList filters = { this.props.filters}
                selectedFilter = { this.state.selectedFilter } />
        </div>
      </div>
    );
  }
});

module.exports = FeedersList;