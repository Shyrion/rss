/*
  Copyright (C) 2015 Jérémy GABRIELE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
  CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

var React       = require('react');
var moment      = require('moment');
var classnames  = require('classnames');
var helper      = require('scripts/clientHelper');

var ArticleDetails = require('components/ArticleDetails/ArticleDetails.jsx');

var ArticleActions  = require('actions/ArticleActions');

var ArticleSummary = React.createClass({
  propTypes: {
    index     : React.PropTypes.number.isRequired,
    guid      : React.PropTypes.string.isRequired,
    feeder    : React.PropTypes.string.isRequired,
    date      : React.PropTypes.instanceOf(Date),
    title     : React.PropTypes.string.isRequired,
    content   : React.PropTypes.string.isRequired,
    author    : React.PropTypes.string,

    selected  : React.PropTypes.bool
  },
  handleClick: function() {
    ArticleActions.selectArticle({ index: this.props.index, guid: this.props.guid }); // Only those two info are needed atm
  },
  render: function() {
    var momentDate = moment(this.props.date);
    var formattedDate = '';
    // TODO: check momentDate.fromNow() to display "XX days ago" if less than 7 days
    formattedDate = momentDate.format("DD MMM YYYY");

    var classes = classnames('article-summary', {
      selected: this.props.selected
    });

    var excerpt = helper.strip(this.props.content);

    if (this.props.selected) {
      return (
        <article onClick={ this.handleClick } className={ classes }>
          <header>
            <span className="feeder">{ this.props.feeder }</span>
            <time className="date" dateTime="2015-07-24">{ formattedDate }</time>
          </header>
          <h3 className="title">
            { this.props.title }
          </h3>
          <p className="content">{ excerpt }</p>
          <ArticleDetails mode =  'display'
                         title = { this.props.title }
                        author = { this.props.author }
                          date = { this.props.date }
                       content = { this.props.content } />
        </article>
      );
    } else {
      return (
        <article onClick={ this.handleClick } className={ classes }>
          <header>
            <span className="feeder">{ this.props.feeder }</span>
            <time className="date" dateTime="2015-07-24">{ formattedDate }</time>
          </header>
          <h3 className="title">
            { this.props.title }
          </h3>
          <p className="content">{ excerpt }</p>
        </article>
      );
    }
  }
});

module.exports = ArticleSummary;