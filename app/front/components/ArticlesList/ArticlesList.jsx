/*
  Copyright (C) 2015 Jérémy GABRIELE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
  CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

var React   = require('react');

var FiltersList    = require('components/FiltersList/FiltersList.jsx');
var ArticleSummary = require('components/ArticlesList/ArticleSummary/ArticleSummary.jsx');

var ArticleStore   = require('stores/ArticleStore');

var mixins = require('scripts/mixins.js');

var ArticlesList = React.createClass({
  mixins: [mixins.FilterManagementMixin],
  getInitialState: function() {
    return {
      articles: ArticleStore.getArticlesToDisplay(),
      selectedArticle: ArticleStore.getselectedArticle()
    }
  },
  handleArticleUpdate2: function(StoreState) { // The global one is handled by mixin
    this.setState({
      articles: StoreState.articlesToDisplay
    });
  },
  handleArticleSelect: function(StoreState) {
    this.setState({
      selectedArticle: StoreState.selectedArticle
    });
  },
  componentDidMount: function() {
    // When a filter has been processed, we want to reorder the articles
    ArticleStore.addArticlesUpdatedListener(this.handleArticleUpdate2);
    // When an article has been selected, we want to update the state of the selected child component
    ArticleStore.addArticleSelectedListener(this.handleArticleSelect);
  },
  componentWillUnmount: function() {
    ArticleStore.removeArticlesUpdatedListener(this.handleArticleUpdate2);
    ArticleStore.removeArticleSelectedListener(this.handleArticleSelect);
  },
  render: function() {
    var allArticles = this.state.articles.map(function(article, index) {
      return (<li>
                <ArticleSummary key = { index }
                              index = { index }
                               guid = { article.guid }
                             feeder = { article.feeder }
                               date = { article.date }
                              title = { article.title }
                             author = { article.author }
                            content = { article.content }
                           selected = { this.state.selectedArticle && (this.state.selectedArticle.index === article.index) } />
              </li>);
    }.bind(this));
    return (
      <div className="articles-list">
        <FiltersList filters = { this.props.filters}
              selectedFilter = { this.state.selectedFilter } />
        <h2 className="hidden">Feeds list</h2>
        <ul className="articles">
          { allArticles }
        </ul>
      </div>
    );
  }
});

module.exports = ArticlesList;