/*
  Copyright (C) 2015 Jérémy GABRIELE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
  CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

var React           = require('react');
var classnames      = require('classnames');
var ArticleActions  = require('actions/ArticleActions');


var Filter = React.createClass({
  propTypes: {
    icon: React.PropTypes.string,
    name: React.PropTypes.string,
    selected: React.PropTypes.bool,
    predicate: React.PropTypes.func
  },
  handleClick: function() {
    ArticleActions.filter(this.props.predicate, this.props.name);
  },
  render: function() {
    var classes = classnames('filter', {
      selected: this.props.selected
    });
    return (
      <div role="option" aria-selected={ this.props.selected } onClick={this.handleClick} className={classes}>
        { this.props.icon || this.props.name }
      </div>
    );
  }
});

module.exports = Filter;