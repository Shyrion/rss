var React = require('react');

var Filter = require('components/FiltersList/Filter/Filter.jsx');

var FiltersList = React.createClass({
  propTypes: {
    filters: React.PropTypes.array.isRequired, // TODO: Array of Filter (define object Filter)
    selectedFilter: React.PropTypes.string
  },
  render: function() {
    var allFilters = this.props.filters.map(function(filter, index) {
      return (<li><Filter key = { index } 
                     name = {filter.name}
                     icon = {filter.icon}
                 selected = { (this.props.selectedFilter === filter.name) || ((this.props.selectedFilter === null) && index === 0)} 
                predicate = {filter.predicate} /></li>);
    }.bind(this));
    return (
      <ul className="filters-list">
        { allFilters }
      </ul>
    );
  }

});

module.exports = FiltersList;