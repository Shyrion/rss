/*
  Copyright (C) 2015 Jérémy GABRIELE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
  CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/
/*
  Copyright (C) 2015 Jérémy GABRIELE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
  CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

var React = require('react');
var moment  = require('moment');

var ArticleStore   = require('stores/ArticleStore');

var ArticleDetails = React.createClass({
  propTypes: {
    title   : React.PropTypes.string,
    author  : React.PropTypes.string,
    date    : React.PropTypes.instanceOf(Date),
    content : React.PropTypes.string,
    mode    : React.PropTypes.string
  },
  getInitialState: function() {
    // State could be removed if component was re-generated on ArticleSelected event (would do that in main.js)
    return {
      title   : this.props.title,
      author  : this.props.author,
      date    : this.props.date,
      content : this.props.content,

      mode    : this.props.mode || 'waiting'
    }
  },
  handleArticleSelect: function(StoreState) {
    var article = StoreState.selectedArticle;
    this.setState({
      title   : article.title,
      author  : article.author,
      date    : article.date,
      content : article.content,

      mode: 'display'
    });
  },
  componentDidMount: function() {
    ArticleStore.addArticleSelectedListener(this.handleArticleSelect);
  },
  componentWillUnmount: function() {
    ArticleStore.removeArticleSelectedListener(this.handleArticleSelect);
  },
  render: function() {
    if (this.state.mode === 'waiting') {
      return (<span className="article-details">Please select an article in the list</span>)
    }


    var momentDate = moment(this.state.date);
    var formattedDate = '';
    // TODO: check momentDate.fromNow() to display "XX days ago" if less than 7 days
    formattedDate = momentDate.format("DD MMM YYYY [at] hh:mm");
    formattedDateForTimeAttribute = momentDate.format("YYYY-MM-DD");
    return (
      <article className="article-details">
        <h2 className="hidden">Article details</h2>
        <h3 className="title">{ this.state.title }</h3>
        <time className="date" dateTime={ formattedDateForTimeAttribute }>{ formattedDate }</time>
        by <span className="author">{ this.state.author }</span>

        <section className="content" dangerouslySetInnerHTML={ { __html: this.state.content } } >

        </section>
      </article>
    );
  }
});

module.exports = ArticleDetails;