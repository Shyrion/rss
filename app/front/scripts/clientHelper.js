var localStorageSupported = localStorage != 'undefined';

module.exports = {
  strip: function(html) {
    var tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
  },

  getLocalStorageItem: function(value, defaultValue) {
    return localStorage && localStorage.getItem(value) || defaultValue;
  }
}