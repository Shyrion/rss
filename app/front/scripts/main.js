/*
  Copyright (C) 2015 Jérémy GABRIELE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
  CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

var React = require('react');

var ArticleDetails  = require('../components/ArticleDetails/ArticleDetails.jsx');
var ArticlesList    = require('../components/ArticlesList/ArticlesList.jsx');
var FeedersList       = require('../components/FeedersList/FeedersList.jsx');


var filters = [
  { name: 'All', predicate: function(article) { return true; }},
  { name: 'Unread', selected: true, predicate: function(article) { return article && (article.read == false); }},
  { name: 'Starred', predicate: function(article) { return article && (article.starred == false); }}
];

React.render(<FeedersList feeders = { [] }
                          filters = { filters } />, document.getElementById('feeders-list-wrapper'));

React.render(<ArticlesList filters = { filters } />, document.getElementById('articles-list-wrapper'));

React.render(<ArticleDetails />, document.getElementById('article-details-wrapper'));