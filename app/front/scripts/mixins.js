/*
  Copyright (C) 2015 Jérémy GABRIELE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
  CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

var React = require('react');

var ArticleStore = require('stores/ArticleStore');

exports.FilterManagementMixin = {
  propTypes: {
    filters: React.PropTypes.array.isRequired // TODO: Array of Filter (define object Filter)
  },
  getInitialState: function() {
    return {
      selectedFilter: ArticleStore.getselectedFilter()
    }
  },
  handleArticleUpdate: function(StoreState) {
    this.setState({
      selectedFilter: StoreState.selectedFilter
    });
  },
  componentDidMount: function() {
    // When a filter has been processed, we want to reorder the articles
    ArticleStore.addArticlesUpdatedListener(this.handleArticleUpdate);
  },
  componentWillUnmount: function() {
    ArticleStore.removeArticlesUpdatedListener(this.handleArticleUpdate);
  }
}