/*
  Copyright (C) 2015 Jérémy GABRIELE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
  CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

var EventEmitter    = require('events').EventEmitter;
var assign          = require('object-assign');
var Dispatcher      = require('scripts/MainDispatcher');
var Constants       = require('scripts/constants');

var clientHelper    = require('scripts/clientHelper');

/*
 * Store state
 * Hold in one single object to facilitate the passing to change listeners
 */
var _StoreState = {
  feedersFullList   : []
}

var FeederStore = assign({}, EventEmitter.prototype, {
  initStore: function() {
    // The full list of articles (fetched from the server)

    var req = new XMLHttpRequest();
    req.open('GET', '/feeders', true);
    req.onreadystatechange = function (aEvt) {
      if (req.readyState == 4) {
        if(req.status == 200) {
          var jsonResult = JSON.parse(req.responseText);
          _StoreState.feedersFullList = jsonResult.map(function(feederInstance, index) {
            return {
              name  : feederInstance.name,
              url   : feederInstance.url
            };
          });

          this.emitFeedersUpdated();
        } else {
          console.error('XHR: Something went wrong');
        }
      }
    }.bind(this);
    req.send(null);
  },

  // Getters
  getFeeders: function() {
    return _StoreState.feedersFullList;
  },

  // Event management
  emitFeedersUpdated: function() {
    this.emit(Constants.Feeder.FEEDERS_UPDATED, _StoreState); // Publish the store state on event
  },
  addFeedersUpdatedListener: function(callback) {
    this.on(Constants.Feeder.FEEDERS_UPDATED, callback);
  },
  removeFeedersUpdatedListener: function(callback) {
    this.removeListener(Constants.Feeder.FEEDERS_UPDATED, callback);
  },

  // Main dispatcher handler
  dispatcherIndex: Dispatcher.register(function(payload) {
    var action = payload.action;

    // Ready for add Feeder feature
    switch(action) {
      default:
        break;
    }
    return true;
  })

});

module.exports = FeederStore;


// Initialize the store
FeederStore.initStore();