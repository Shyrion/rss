/*
  Copyright (C) 2015 Jérémy GABRIELE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
  CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

var EventEmitter    = require('events').EventEmitter;
var assign          = require('object-assign');
var Dispatcher      = require('scripts/MainDispatcher');
var Constants       = require('scripts/constants');

var clientHelper    = require('scripts/clientHelper');

/*
 * Store state
 * Hold in one single object to facilitate the passing to change listeners
 */
var _StoreState = {
  selectedFilter    : null,
  selectedArticle   : null,
  articlesFullList  : [],
  articlesToDisplay : [],
  articlesRead      : localStorage && localStorage.getItem('articlesRead'),
  articlesStarred   : localStorage && localStorage.getItem('articlesStarred'),
}

var ArticleStore = assign({}, EventEmitter.prototype, {
  initStore: function() {
    // The full list of articles (fetched from the server)

    var req = new XMLHttpRequest();
    req.open('GET', '/articles', true);
    req.onreadystatechange = function (aEvt) {
      if (req.readyState == 4) {
        if(req.status == 200) {
          var jsonResult = JSON.parse(req.responseText);
          _StoreState.articlesFullList = jsonResult.map(function(articleInstance, index) {
            return {
              index     : index,
              guid      : articleInstance.guid,
              feeder    : articleInstance.feeder,
              title     : articleInstance.title,
              author    : articleInstance.author,
              date      : new Date(articleInstance.date),
              content   : articleInstance.description,
              read      : ArticleStore.isArticleRead(articleInstance.guid),
              starred   : ArticleStore.isArticleStarred(articleInstance.guid)
            };
          });

          // The displayed articles list
          _StoreState.articlesToDisplay = _StoreState.articlesFullList.slice(); // By default, copy the full list
          this.emitArticlesUpdated();
        } else {
          console.error('XHR: Something went wrong');
        }
      }
    }.bind(this);
    req.send(null);
  },


  // Getters
  getArticlesToDisplay: function() {
    return _StoreState.articlesToDisplay;
  },
  getselectedFilter: function() {
    return _StoreState.selectedFilter;
  },
  getselectedArticle: function() {
    return _StoreState.selectedArticle;
  },


  // Event management
  emitArticlesUpdated: function() {
    this.emit(Constants.Article.ARTICLES_UPDATED, _StoreState); // Publish the store state on event
  },
  addArticlesUpdatedListener: function(callback) {
    this.on(Constants.Article.ARTICLES_UPDATED, callback);
  },
  removeArticlesUpdatedListener: function(callback) {
    this.removeListener(Constants.Article.ARTICLES_UPDATED, callback);
  },

  emitArticlesSelected: function() {
    this.emit(Constants.Article.ARTICLES_SELECTED, _StoreState);
  },
  addArticleSelectedListener: function(callback) {
    this.on(Constants.Article.ARTICLES_SELECTED, callback);
  },
  removeArticleSelectedListener: function(callback) {
    this.removeListener(Constants.Article.ARTICLES_SELECTED, callback);
  },

  // Business functions
  filterArticles: function(predicate, filterName) {
    _StoreState.articlesToDisplay = _StoreState.articlesFullList.filter(function(article, index) {
      return predicate(article);
    });
    _StoreState.selectedFilter = filterName;
  },
  selectArticle: function(article) {
    _StoreState.selectedArticle = _StoreState.articlesFullList[article.index];
    this.addReadArticle(article.guid);
  },
  isArticleRead: function(articleGuid) {
    var storedArticlesRead = clientHelper.getLocalStorageItem('articlesRead', '').split(',');
    if (storedArticlesRead) {
      return (storedArticlesRead.indexOf(articleGuid) != -1);
    } else {
      return false;
    }
  },
  isArticleStarred: function(articleGuid) {
    var storedArticlesStarred = clientHelper.getLocalStorageItem('articlesStarred', '').split(',');
    if (storedArticlesStarred) {
      return (storedArticlesStarred.indexOf(articleGuid) != -1);
    } else {
      return false;
    }
  },
  addReadArticle: function(articleGuid) {
    var storedArticlesRead = clientHelper.getLocalStorageItem('articlesRead', '').split(',');
    if (storedArticlesRead) {
      storedArticlesRead.push(articleGuid);
      localStorage.setItem('articlesRead', storedArticlesRead);
      _StoreState.articlesRead = storedArticlesRead;
    }
  },
  addStarredArticle: function(articleGuid) {
    var storedArticlesStarred = clientHelper.getLocalStorageItem('articlesStarred', '').split(',');
    if (storedArticlesStarred) {
      storedArticlesStarred.push(articleGuid);
      localStorage.setItem('articlesStarred', storedArticlesStarred);
      _StoreState.articlesRead = storedArticlesStarred;
    }
  },

  // Main dispatcher handler
  dispatcherIndex: Dispatcher.register(function(payload) {
    var action = payload.action;

    switch(action) {
      case Constants.Article.FILTER:
        var predicate = payload.predicate;
        var filterName = payload.filterName;
        ArticleStore.filterArticles(predicate, filterName);
        ArticleStore.emitArticlesUpdated();
        break;
      case Constants.Article.SELECT_ARTICLE:
        var article = payload.article;
        ArticleStore.selectArticle(article);
        ArticleStore.emitArticlesSelected();
        break;
      default:
        break;
    }
    return true;
  })

});

module.exports = ArticleStore;


// Initialize the store
ArticleStore.initStore();