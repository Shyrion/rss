/*
  Copyright (C) 2015 Jérémy GABRIELE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
  CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

var ArticlesController = require('../app/controllers/ArticlesController');
var FeedersController  = require('../app/controllers/FeedersController');

module.exports = function(app) {

  //=========================//
  //========= HOME ==========//
  //=========================//

  app.get('/', function(req, res) {
    res.render('index');
  });

  //=========================//
  //======= ARTICLES ========//
  //=========================//

  app.get('/articles', function(req, res) {
    ArticlesController.getAllArticles(function(err, allArticles) {
      res.send(allArticles);
    });
  });

  app.get('/articles/update', function(req, res) {
    FeedersController.getAllFeeders(function(err, allFeeders) {
      var allFeedersObjects = allFeeders.map(function(feeder) { return {name: feeder.name, url: feeder.url} });
      ArticlesController.fetchDataAndCreateArticles(allFeedersObjects, function(success) {
        res.redirect('/');
      });
    });
  });
  app.get('/articles/flush', function(req, res) {
    ArticlesController.removeAllArticles(function(success) {
      res.render('index');
    });
  });

  //=========================//
  //======== FEEDERS ========//
  //=========================//


  app.get('/feeders', function(req, res) {
    FeedersController.getAllFeeders(function(err, allFeeders) {
      res.send(allFeeders);
    });
  });

  //=========================//
  //========== 404 ==========//
  //=========================//

  app.get('*', function(req, res, next) {
    var err = new Error();
    err.status = 404;
    next(err);
  });

  app.use(function(err, req, res, next){
    if(err.status !== 404) {
      return next();
    }
   
    res.send(err.message || '404 - Not found');
  });
}