/*
  Copyright (C) 2015 Jérémy GABRIELE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
  CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

var gulp        = require('gulp');
var sass        = require('gulp-sass');
var cssmin      = require('gulp-cssmin');
var concat      = require('gulp-concat');
var sourcemaps  = require('gulp-sourcemaps');
var uglify      = require('gulp-uglify');

var browserify  = require('browserify');
var reactify    = require('reactify');
var source      = require('vinyl-source-stream');
var buffer      = require('vinyl-buffer');

var livereload  = require('gulp-livereload');

/*
 * General error handler not to make gulp silently fail
 */
function handleError(err) {
  console.error('Error:', err.message);
  this.emit('end');
}

// Default task run setup.
// Running "gulp" is ugly, "gulp setup" looks more like a setup step
gulp.task('default', ['setup']);

// Setup = sassify and reactify
gulp.task('setup', ['sass', 'js', 'copy-images', 'copy-fonts']);


/*
 * sass task
 * 
 * Take all .scss files (mainly from components), sassify them and merge into one file
 * 
 */
gulp.task('sass', function() {
  gulp.src('app/front/**/*.scss')
    .pipe(sourcemaps.init({loadMaps: true}))
      .pipe(concat('main.css'))
      .pipe(sass()).on('error', handleError)
      .pipe(cssmin())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('public/style'))
    .pipe(livereload());
});


/*
 * js task
 * 
 * Use browserify to reactify the .jsx components
 * 
 */
gulp.task('js', function() {
    var bundler = browserify({
        entries: ['./app/front/scripts/main.js'], // Only need initial file, browserify finds the deps
        transform: [reactify], // We want to convert JSX to normal javascript
        paths: ['./app/front/'],
        debug: true // Gives us sourcemapping
    });

    return bundler.bundle().on('error', handleError)
      .pipe(source('main.js'))
      .pipe(buffer()) // converts from streaming to buffered vinyl file object
      .pipe(uglify())
      .pipe(gulp.dest('./public/scripts/'));
});


gulp.task('copy-images', function() {
  gulp.src('app/front/images/*')
    .pipe(gulp.dest('public/images'))
    .pipe(livereload());
});

gulp.task('copy-fonts', function() {
  gulp.src('app/front/fonts/*')
    .pipe(gulp.dest('public/fonts'))
    .pipe(livereload());
});


/*
 * watch task
 * 
 * Watch any change in js files and run the js task
 * Watch any change in scss files and run the sass task
 * 
 */
gulp.task('watch', function() {
  livereload.listen();
  gulp.watch('app/front/**/*.scss', ['sass']);
  gulp.watch('app/front/**/*.js*', ['js']);
  gulp.watch('app/front/**/*.jsx', ['js']);
  gulp.watch('app/front/images/*', ['copy-images']);
  gulp.watch('app/front/fonts/*', ['copy-fonts']);
});