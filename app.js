/*
  Copyright (C) 2015 Jérémy GABRIELE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
  CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

var express         = require('express');
var bodyParser      = require('body-parser');
var mongoose        = require('mongoose');

/*
 * Setup the Mongoose connection
 */
function setupMongoose(callback) {
  mongoose.connect('mongodb://localhost/rss', function(err) {
    if (err) {
      console.error("COULD NOT START SERVER : ", err);
      process.exit(0);
    } else {
      mongoose.connection.on('error', function(err, a) {
        console.error(err, a);
      });
      if (callback) callback();
    }
  });
}

/*
 * Setup the GET/POST routes from the route.js file
 */
function setupRoutes(app, callback) {
  // Routes setup
  require('./config/routes')(app);

  if (callback) callback(app);
}


/*
 * Setup Express.js, with all of the wanted settings
 */
function setupExpress(callback) {

  var app = express();

  //===== Configuration =====//

  // On my server, there is nginx as a reverse proxy.
  // It allows me to get real IP (and not nginx's 127.0.0.1)
  app.set('trust proxy', true);

  // View engine is ejs. I find it really simple, it does the job...
  // And honestly, not so much used, we have client side templating ;).
  app.set('view engine', 'ejs');
  app.set('views', __dirname + '/app/views');
  app.set('view cache', false);

  app.use(express.static(__dirname + "/public"));

  // Use bodyparser for server-side forms management
  app.use(bodyParser.urlencoded({ extended: false }));

  app.set('port', 3108);

  if (callback) callback(app);
}

function startServer(app) {
  app.listen(app.get('port'), function() {
    console.log('Server is running on port ' + app.get('port'), '(', new Date(), ')');
  });
}

setupMongoose(function() {
  setupExpress(function(app) {
    setupRoutes(app, function(app) {
      startServer(app);
    });
  });
});


// Setup of DB

var ArticlesController = require('./app/controllers/ArticlesController');
var FeedersController  = require('./app/controllers/FeedersController');

var defaultFeeders = [
  { name: 'Kottke.org', url: 'http://feeds.kottke.org/main' },
  { name: 'Bitbucket', url: 'https://bitbucket.org/Shyrion/rss/rss?token=46caee95b289db1de0973b287f9282ef' },
  { name: 'Mashable', url: 'http://feeds.mashable.com/Mashable?format=xml' }
];
var nbToProcess = defaultFeeders.length;
FeedersController.getAllFeeders(function(err, allFeeders) {
  defaultFeeders.forEach(function(feeder) {
    FeedersController.addFeeder(feeder.name, feeder.url, function(err, feederInstance) {
      nbToProcess--;
      console.log(feeder.name, (err && err.code === 11000) && 'Already there' || err || 'Added');

      if (nbToProcess <= 0) {
        ArticlesController.fetchDataAndCreateArticles(defaultFeeders, function(success) {
          console.log('Articles fetched. Initialisation finished');
        });
      }
    });
  });
});